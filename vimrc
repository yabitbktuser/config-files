" Make external commands work through a pipe instead of a pseudo-tty
"set noguipty

" Use Vim settings, rather than Vi settings (much better!).
" This must be first, because it changes other options as a side effect.
set nocompatible

" This shows what you are typing as a command.  I love this!
set showcmd

" Needed for Syntax Highlighting and stuff
filetype on
filetype plugin on
syntax enable
set grepprg=grep\ -nH\ $*

" auto/smart indent
set autoindent
set autoindent smartindent

" Spaces are better than a tab character
set expandtab
set smarttab

set enc=utf-8
set fenc=utf-8
set termencoding=utf-8

" Needed for Syntax Highlighting and stuff
filetype on
filetype plugin on
syntax enable
syntax on
set grepprg=grep\ -nH\ $*

" show cursor position
set ruler

" Spaces are better than a tab character
set expandtab
set smarttab

set tabstop=2
set shiftwidth=2
set softtabstop=2

set cinwords=if,else,while,do,for,switch,case
set tags+=~/.vim/tags/tonka
set tags+=~/.vim/tags/cpp

" allow backspacing over everything in insert mode
set backspace=indent,eol,start

" show line numbers
set number

" show matching brackets
set showmatch

" do case insensitive search
set ignorecase

" do smart case search
set smartcase

" Enable mouse support in console
set mouse=a

" Hide the mouse when typing text
set mousehide

" Enable incremental search
set incsearch

" Also switch on highlighting the last used search pattern
set hlsearch

" remove the buffer, when a tab is closed
set nohidden

set backup
set backupdir=~/vimfiles/backup

" keep at least 5 lines above/below
set scrolloff=3

" keep at least 5 lines left/right
set sidescrolloff=5
set history=200

" 1000 undos
set undolevels=1000

set wildmode=longest:full

" menu has tab completion
set wildmenu

" Favorite Color Scheme
if has("gui_running")
  set t_Co=256
  colorscheme wombat256mod
  " colorscheme blackboard

  " Remove Toolbar
   set guioptions-=T

   "Terminus
   set guifont=Consolas:h11
endif

execute pathogen#infect()

" autocmd vimenter * if !argc() | NERDTree | endif
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTreeType") && b:NERDTreeType == "primary") | q | end

" shortcut keys

"Ctrl+n - open NERDtree
map <C-n> :NERDTreeToggle<CR>
