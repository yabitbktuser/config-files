# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

BOOST_ROOT=/usr/local/include
QT_DIR=/usr/local/Trolltech/Qt-4.6.2/bin
GOOGLE_APPENGINE_DIR=/usr/local/google_appengine
VIMRUNTIME=~/.vim

PATH=/usr/kerberos/bin:/bin:/usr/bin:/usr/sbin:/usr/ucb:/home/aarumug/.local/bin:/usr/local/bin:/usr/local/sbin

PATH=$PATH:$BOOST_ROOT:$QT_DIR:$VIMRUNTIME:$GOOGLE_APPENGINE_DIR
export PATH

export LESS=-R

LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib
export LD_LIBRARY_PATH

# User specific aliases and functions
alias ll="ls -l --color=tty"
alias la="ll -A"
alias grep='grep --color=always'
alias lsd='ll | grep ^d'
alias lsf='ll | grep -v ^d'
alias cdtnz="cd ~/git_repos/tnz_source"
alias mkdir='mkdir -v'
alias cp='cp -v'
alias mv='mv -v'
alias cdmt='cd ~/work/mira/trunk'
